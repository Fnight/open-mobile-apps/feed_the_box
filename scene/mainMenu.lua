--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
======FEEDTHEBOX========
====COPYRIGHT FNIGHT====
========================
]]--

local composer = require("composer")
local scene = composer.newScene()


function scene:create(event)

	local sceneGroup = self.view

	local TITLE_COLOR = {222/255, 94/255, 97/255}
	local MENU_TEXT_COLOR = {74/255, 53/255, 23/255}
	local MENU_ICON_COLOR = {92/255, 57/255, 39/255}


	local background = display.newImageRect( sceneGroup, "images/bg_menu.png", display.actualContentWidth, display.actualContentWidth*2 )
	background.x, background.y = display.contentCenterX, display.contentCenterY


	local title = display.newText( sceneGroup, "Feed The Box", display.contentCenterX, 150, FONT, 125)
	title:setFillColor(unpack(TITLE_COLOR))


	local playButtonGroup = display.newGroup()
	sceneGroup:insert(playButtonGroup)
	playButtonGroup.x, playButtonGroup.y = display.contentCenterX, 400
	playButtonGroup.rotation = -20

	local playButton = display.newImageRect( sceneGroup, "images/baguette.png", 400, 400)
	playButton.rotation = 45
	playButtonGroup:insert(playButton)

	local playButton_text = display.newText( sceneGroup, "Game", playButton.x, playButton.y + 50, FONT, 125)
	playButton_text:setFillColor(unpack(MENU_TEXT_COLOR))
	playButtonGroup:insert(playButton_text)


	local shopButtonGroup = display.newGroup()
	sceneGroup:insert(shopButtonGroup)
	shopButtonGroup.x, shopButtonGroup.y = display.contentCenterX, 700
	shopButtonGroup.rotation = 20

	local shopButton = display.newImageRect( sceneGroup, "images/baguette.png", 400, 400)
	shopButton.rotation = 45
	shopButtonGroup:insert(shopButton)

	local shopButton_text = display.newText( sceneGroup, "Shop", shopButton.x, shopButton.y + 50, FONT, 125)
	shopButton_text:setFillColor(unpack(MENU_TEXT_COLOR))
	shopButtonGroup:insert(shopButton_text)
	

	local function rotateNext()
		transition.to(playButtonGroup, {time = 2000, rotation = - playButtonGroup.rotation, onComplete = rotateNext})
		transition.to(shopButtonGroup, {time = 2000, rotation = - shopButtonGroup.rotation})
	end

	rotateNext()


	local soundBurger = display.newImageRect( sceneGroup, "images/menuIcon.png", 144, 150)
	soundBurger.x, soundBurger.y = display.contentCenterX - 200, 1100

	local soundBurger_icon = display.newImageRect( sceneGroup, "images/speaker.png", 90, 90)
	soundBurger_icon:setFillColor(unpack(MENU_ICON_COLOR))
	soundBurger_icon.x, soundBurger_icon.y = soundBurger.x, soundBurger.y


	local musicBurger = display.newImageRect( sceneGroup, "images/menuIcon.png", 144, 150)
	musicBurger.x, musicBurger.y = display.contentCenterX + 200, 1100

	local musicBurger_icon = display.newImageRect( sceneGroup, "images/music.png", 90, 90)
	musicBurger_icon:setFillColor(unpack(MENU_ICON_COLOR))
	musicBurger_icon.x, musicBurger_icon.y = musicBurger.x, musicBurger.y

	local isGo = false

	local function go_to_game()
		if (isGo == false) then
			isGo = true
			if (baseTable.sound) then audio.play(clickSound) end
			transition.cancel()
			gotoScene("scene.game","fromRight")
		end
	end


	local function go_to_shop()
		if (isGo == false) then
			isGo = true
			if (baseTable.sound) then audio.play(clickSound) end
			transition.cancel()
			gotoScene("scene.shop","fromRight")
		end
	end

	shopButton:addEventListener("tap",go_to_shop)
	playButton:addEventListener("tap", go_to_game)


end

function scene:destroy(event)
	local sceneGroup = self.view
	display.remove(sceneGroup)
	sceneGroup = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("destroy", scene)

return scene
