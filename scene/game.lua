--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
======FEEDTHEBOX========
====COPYRIGHT FNIGHT====
========================
]]--

local composer = require("composer")
local scene = composer.newScene()
local physics = require("physics")


function scene:create(event)

	local sceneGroup = self.view

	local TITLE_COLOR = {222/255, 94/255, 97/255}
	local MENU_TEXT_COLOR = {74/255, 53/255, 23/255}
	local MENU_ICON_COLOR = {92/255, 57/255, 39/255}

	physics.start()
	--physics.setDrawMode("hybrid")
	--physics.setDrawMode("debug")

	local isPause = false
	local isDeath = false
	local money = baseTable.money
	local score = 0
	local localTime = 0

	local categoriesForLevel = {}
	for i = 1,#CATEGORIES do
		if (baseTable.boughtCategorie[i] == 1) then table.insert(categoriesForLevel, i) end
	end

	local trashForLevel = {}
	for i = 1,#TRASH do
		if (baseTable.boughtTrash[i] == 1) then table.insert(trashForLevel, i) end
	end

	print(#categoriesForLevel)
	print(#trashForLevel)

	local forDeleteGroup = display.newGroup()
	--sceneGroup:insert(forDeleteGroup)

	local background = display.newImageRect( sceneGroup, "images/bg_game.png", display.actualContentWidth, display.actualContentWidth*2 )
	background.x, background.y = display.contentCenterX, display.contentCenterY


	local cart = display.newImageRect( sceneGroup, "images/online-store.png", 300, 300)
	cart.rotation = 180
	cart.x, cart.y = display.contentCenterX, 100


	local score_text = display.newText(sceneGroup, score,  display.screenOriginX + 100, 135, FONT, 125)
	score_text:setFillColor(unpack(TITLE_COLOR))
	score_text.alpha = 0.8


	local moneyIcon = display.newImageRect( sceneGroup, "images/dollar.png", 60, 60)
	moneyIcon.x, moneyIcon.y = display.screenOriginX + 90, 200
	moneyIcon.alpha = 0

	local moneyText = display.newText( sceneGroup, money, display.screenOriginX + 200, 235, FONT, 125)
	moneyText:setFillColor(106/255, 194/255, 89/255)
	moneyText.alpha = 0


	local recordScoreText = display.newText( sceneGroup, "Best score: "..baseTable.recordScore, display.contentCenterX, 900, FONT, 100)
	recordScoreText:setFillColor(unpack(TITLE_COLOR))
	recordScoreText.alpha = 0


	local recycleBin = display.newImageRect( sceneGroup, "images/recycle-bin.png", 230, 230)
	recycleBin.x, recycleBin.y = display.contentCenterX + 240, 1115
	physics.addBody(recycleBin, "static", 
		{ friction = 1, bounce = 0, shape = {-115,-100,-85,100,-105,-100,-75,100} },
		{ friction = 1, bounce = 0, shape = {85,100,115,-100,75,100,105,-100} }
	)
	recycleBin.type = "box"
	forDeleteGroup:insert(recycleBin)

	local recycleBin_deleter = display.newRect( sceneGroup, display.contentCenterX + 240, 1215, 140, 5)
	physics.addBody(recycleBin_deleter, "static")
	recycleBin_deleter.type = "deleter_trash"
	recycleBin_deleter.isVisible = false
	forDeleteGroup:insert(recycleBin_deleter)


	local foodBox = display.newImageRect( sceneGroup, "images/leeves.png", 200, 220)
	foodBox.x, foodBox.y = display.contentCenterX - 240, 1115
	physics.addBody(foodBox, "static", 
		{ friction = 1, bounce = 0, shape = {-100,-100,-85,100,-90,-100,-75,100} },
		{ friction = 1, bounce = 0, shape = {85,100,100,-100,75,100,90,-100} }
	)
	foodBox.type = "box"
	forDeleteGroup:insert(foodBox)

	local foodBox_deleter = display.newRect( sceneGroup, display.contentCenterX - 240, 1215, 140, 5)
	physics.addBody(foodBox_deleter, "static")
	foodBox_deleter.type = "deleter_food"
	foodBox_deleter.isVisible = false
	forDeleteGroup:insert(foodBox_deleter)


	local desk = display.newRect( sceneGroup, display.contentCenterX, 900, 300, 50)
	desk:setFillColor(150/255, 75/255, 0)
	physics.addBody(desk, "static", { friction=0 })
	desk.rotation = 45
	desk.type = "desk"
	forDeleteGroup:insert(desk)

	local deskController = display.newRect( sceneGroup, display.contentCenterX, 900, 500, 500)
	deskController.isVisible = false
	deskController.isHitTestable = true

	local function rotateDesk()
		if (desk.rotation > 0) then
			transition.to(desk, {time = 250, rotation = -45})
		else
			transition.to(desk, {time = 250, rotation = 45})
		end
	end

	deskController:addEventListener("tap",rotateDesk)


	local function onLocalCollision( self, event )
		if ( event.phase == "began" ) then
			if (self.type == "food" and event.other.type == "deleter_food") then
				self:removeSelf()
				score = score + 1
				score_text.text = score
				print("Доставка еды")
			end
			if (self.type == "trash" and (event.other.type == "deleter_trash" or event.other.type == "floor")) then
				self:removeSelf()
				print("Мусор улетел "..event.other.type)
			end
			if ((self.type == "food" and (event.other.type == "deleter_trash" or event.other.type == "floor")) or (self.type == "trash" and event.other.type == "deleter_food")) then
				isDeath = true
				physics.pause()
				deskController:removeEventListener("tap", rotateDesk)
				if (score > baseTable.recordScore) then
					recordScoreText.text = "new record!"
					baseTable.recordScore = score
				end
				recordScoreText:toFront()
				baseTable.money = money
				saveBase()
				transition.to(recordScoreText, {time = 250, alpha = 1})
				transition.to(homeButton, {time = 250, alpha = 1})
				transition.to(pauseButton, {time = 250, alpha = 0})
				transition.to(score_text, {time = 250, x = display.contentCenterX, y = display.contentCenterY, size = 500, alpha = 1})
				print("Косяк, засунул "..self.type.." в "..event.other.type)
			end
			if (self.type == "money" and event.other.type ~= "desk" and event.other.type ~= "box") then
				self:removeSelf()
			end
		end
	end


	local function gameLoop()
		if (isPause == false and isDeath == false) then
			if (localTime % 20 == 0) then
				if (math.random() > 0.5) then
					local randomChosenCategorie = categoriesForLevel[math.random(1,#categoriesForLevel)]
					local randomChosenFood = math.random(1,#CATEGORIES[randomChosenCategorie])
					local tempObject = display.newImageRect( sceneGroup, "images/"..CATEGORIES[randomChosenCategorie][randomChosenFood], 100, 100)
					tempObject.x, tempObject.y = cart.x, cart.y - 200
					tempObject.type = "food"
					tempObject.collision = onLocalCollision
					tempObject:addEventListener( "collision" )
					tempObject.rotation = math.random(0, 360)
					local tempOutline = graphics.newOutline( 15, "images/"..CATEGORIES[randomChosenCategorie][randomChosenFood] )
					physics.addBody(tempObject, {friction=0.5, outline=tempOutline})
					forDeleteGroup:insert(tempObject)
				--elseif (math.random() > 0.9) then
				elseif (math.random() > 0.1) then
					local randomChosenCategorie = trashForLevel[math.random(1,#trashForLevel)]
					local randomChosenTrash = math.random(1,#TRASH[randomChosenCategorie])
					local tempObject = display.newImageRect( sceneGroup, "images/"..TRASH[randomChosenCategorie][randomChosenTrash], 100, 100)
					tempObject.x, tempObject.y = cart.x, cart.y - 200
					tempObject.type = "trash"
					tempObject.collision = onLocalCollision
					tempObject:addEventListener( "collision" )
					local tempOutline = graphics.newOutline( 15, "images/"..TRASH[randomChosenCategorie][randomChosenTrash] )
					physics.addBody(tempObject, {friction=0.5, outline=tempOutline})
					tempObject.rotation = math.random(0, 360)
					forDeleteGroup:insert(tempObject)
				else
					local tempObject = display.newImageRect( sceneGroup, "images/money.png", 100, 100)
					tempObject.x, tempObject.y = cart.x, cart.y - 200
					tempObject.type = "money"
					tempObject.collision = onLocalCollision
					tempObject:addEventListener( "collision" )
					local tempOutline = graphics.newOutline( 15, "images/money.png" )
					physics.addBody(tempObject, {friction=0.5, outline=tempOutline})
					tempObject.rotation = math.random(0, 360)
					forDeleteGroup:insert(tempObject)

					local function collectMoney(event)
						local function transitionBack()
							transition.to(moneyText, {alpha = 0, time = 200, delay = 2000})
							transition.to(moneyIcon, {alpha = 0, time = 200, delay = 2000})
							event.target:removeSelf()
						end
						event.target.isAwake = false
						event.target:removeEventListener("touch", collectMoney)
						event.target:applyForce(-100, -100, event.target.x, event.target.y)
						money = money + 1
						moneyText.text = money
						moneyText.x = moneyText.width + moneyIcon.x
						transition.to(moneyText, {alpha = 1, time = 200, delay = 200, onComplete = transitionBack})
						transition.to(moneyIcon, {alpha = 1, time = 200, delay = 200})
						print("Собрали денежку, теперь их "..money)
					end

					tempObject:addEventListener("touch", collectMoney)
				end
				cart:toFront()
				foodBox:toFront()
				recycleBin:toFront()
			end
			localTime = localTime + 1
		end
	end

	local timerGameLoop = timer.performWithDelay( 100, gameLoop, 0 )


	local floor = display.newRect( sceneGroup, display.contentCenterX, 1280, 3000, 100)
	physics.addBody(floor, "static", { friction=1, bounce=0.5 })
	floor.type = "floor"
	forDeleteGroup:insert(floor)

	sceneGroup:insert(forDeleteGroup)


	local function setPause()
		pauseButton:toFront()
		playButton:toFront()
		pauseButton.isHitTestable = true
		if (isPause == false) then
			isPause = true
			physics.pause()
			timer.pause(timerGameLoop)
			deskController:removeEventListener("tap", rotateDesk)
			transition.to(pauseButton, {time = 250, x = display.contentCenterX, y = display.contentCenterY, width = 500, height = 500, alpha = 0})
			transition.to(homeButton, {time = 250, alpha = 1})
			transition.to(playButton, {time = 250, x = display.contentCenterX, y = display.contentCenterY, width = 500, height = 500, alpha = 1})
		else
			isPause = false
			physics.start()
			timer.resume(timerGameLoop)
			deskController:addEventListener("tap",rotateDesk)
			transition.to(pauseButton, {time = 250, x = 720 - display.screenOriginX - 100, y = 100, width = 100, height = 100, alpha = 0.8})
			transition.to(playButton, {time = 250, x = 720 - display.screenOriginX - 100, y = 100, width = 100, height = 100, alpha = 0})
			transition.to(homeButton, {time = 250, alpha = 0})
		end
	end

	pauseButton = display.newImageRect( sceneGroup, "images/pause-button.png", 100, 100)
	pauseButton:setFillColor(unpack(TITLE_COLOR))
	pauseButton.x, pauseButton.y = 720 - display.screenOriginX - 100, 100
	pauseButton.alpha = 0.8
	pauseButton:addEventListener("tap", setPause)

	playButton = display.newImageRect( sceneGroup, "images/play-button.png", 100, 100)
	playButton.x, playButton.y = 720 - display.screenOriginX - 100, 100
	playButton:setFillColor(unpack(TITLE_COLOR))
	playButton.alpha = 0


	local function go_to_menu()
		transition.cancel()
		timer.cancel(timerGameLoop)
		forDeleteGroup:removeSelf()
		if (baseTable.sound) then audio.play(clickSound) end
		gotoScene("scene.mainMenu","fromLeft")
	end


	homeButton = display.newImageRect( sceneGroup, "images/home.png", 200, 200)
	homeButton:setFillColor(unpack(TITLE_COLOR))
	homeButton.x, homeButton.y = display.contentCenterX, 1050
	homeButton.alpha = 0
	homeButton:addEventListener("tap", go_to_menu)



end

function scene:destroy(event)
	local sceneGroup = self.view
	display.remove(sceneGroup)
	sceneGroup = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("destroy", scene)

return scene
