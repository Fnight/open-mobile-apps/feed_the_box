--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
======FEEDTHEBOX========
====COPYRIGHT FNIGHT====
========================
]]--

local composer = require("composer")
local scene = composer.newScene()
local physics = require("physics")


function scene:create(event)

	local sceneGroup = self.view

	local categorie_open = false

	local TITLE_COLOR = {222/255, 94/255, 97/255}
	local MENU_TEXT_COLOR = {74/255, 53/255, 23/255}
	local MENU_ICON_COLOR = {92/255, 57/255, 39/255}


	physics.start()
	--physics.setDrawMode("hybrid")
	--physics.setDrawMode("debug")


	local background = display.newImageRect( sceneGroup, "images/bg_game.png", display.actualContentWidth, display.actualContentWidth*2 )
	background.x, background.y = display.contentCenterX, display.contentCenterY


	local title = display.newText( sceneGroup, "Shop", display.contentCenterX, 150, FONT, 125)
	title:setFillColor(unpack(TITLE_COLOR))


	local bigCircle = display.newCircle( sceneGroup, display.contentCenterX, display.contentCenterY, 0) --350
	bigCircle:setFillColor( 1, 0.5) 
	bigCircle:setStrokeColor(unpack(TITLE_COLOR))
	bigCircle.strokeWidth = 16
	bigCircle.isVisible = false

	local chainList = {}

	local countVertex = 360

	for i = 0,countVertex/2 + 1 do
		table.insert(chainList, -350 + ((700/(countVertex/2))*i) )
		table.insert(chainList, math.sqrt(350*350 - chainList[#chainList]*chainList[#chainList]) )
	end
	for i = 0 ,countVertex/2 + 1 do
		table.insert(chainList, -350 + ((700/(countVertex/2))*i) )
		table.insert(chainList, -math.sqrt(350*350 - chainList[#chainList]*chainList[#chainList]) )
	end


	physics.addBody(bigCircle, "static", 
		{
			chain=chainList,
        	connectFirstAndLastChainVertex = true,
        	friction = 1,
        	bounce = 0.1
		}
	)



	local closeButton_group = display.newGroup()
	closeButton_group.x, closeButton_group.y = display.contentCenterX - 720, display.contentCenterY + 500
	sceneGroup:insert(closeButton_group)

	local closeButton = display.newRoundedRect( sceneGroup, 0, 0, 350, 100, 15)
	closeButton:setFillColor(unpack(TITLE_COLOR))
	closeButton_group:insert(closeButton)

	local closeButton_text = display.newText( sceneGroup, "Close", 0, 20, FONT, 75)
	closeButton_text:setFillColor(unpack(MENU_ICON_COLOR))
	closeButton_group:insert(closeButton_text)


	local setButton_group = display.newGroup()
	setButton_group.x, setButton_group.y = display.contentCenterX + 720, display.contentCenterY + 500
	sceneGroup:insert(setButton_group)

	local setButton = display.newRoundedRect( sceneGroup, 0, 0, 350, 100, 15)
	setButton:setFillColor(unpack(TITLE_COLOR))
	setButton_group:insert(setButton)

	local setButton_text = display.newText( sceneGroup, "Buy", 0, 20, FONT, 75)
	setButton_text:setFillColor(unpack(MENU_ICON_COLOR))
	setButton_group:insert(setButton_text)

	local circleGroup = display.newGroup()
	sceneGroup:insert(circleGroup)


	local function closeCategorie()
		local function hideObj()
			bigCircle.isVisible = false
			transition.to(setButton_group, {time = 200, x = display.contentCenterX + 720})
			transition.to(closeButton_group, {time = 200, x = display.contentCenterX - 720})
			categorie_open = false
		end
		if (categorie_open) then
			currentCategorie:removeSelf()
			transition.to(bigCircle.path, {time = 250, radius = 0, onComplete = hideObj})
			circleGroup.isHitTestable = true
		end
	end

	
	local function openCategorieFood(event)
		if (categorie_open == false) then
			print("Открыли категорию "..event.target.categorie)
			if (baseTable.boughtCategorie[event.target.categorie] == 1) then
				if (baseTable.setCategorie[event.target.categorie] == 1) then
					setButton_text.text = "Switch off"
				else
					setButton_text.text = "Switch on"
				end
			else
				setButton_text.text = "Buy"
			end

			bigCircle.isVisible = true
			bigCircle:toFront()
			transition.to(bigCircle.path, {time = 250, radius = 350})
			transition.to(setButton_group, {time = 200, x = display.contentCenterX + 200})
			transition.to(closeButton_group, {time = 200, x = display.contentCenterX - 200})
			categorie_open = true
			currentCategorie = display.newGroup()
			sceneGroup:insert(currentCategorie)
			for i = 1, #CATEGORIES[event.target.categorie] do
				local tempObject = display.newImageRect( sceneGroup, "images/"..CATEGORIES[event.target.categorie][i], 100, 100)
				tempObject.x, tempObject.y = math.random(-300/math.sqrt(2), 300/math.sqrt(2)) + display.contentCenterX, math.random(-300/math.sqrt(2), 300/math.sqrt(2)) + display.contentCenterY
				local tempOutline = graphics.newOutline( 15, "images/"..CATEGORIES[event.target.categorie][i] )
				physics.addBody(tempObject, {friction=1, bounce=0.1, outline=tempOutline})	
				tempObject.rotation = math.random(0, 360)
				currentCategorie:insert(tempObject)
			end
			bigCircle:toFront()
			setButton_group:toFront()
			closeButton_group:toFront()
			circleGroup.isHitTestable = false
		end
	end

	local function openCategorieTrash(event)
		if (categorie_open == false) then
			print("Открыли категорию "..event.target.trash)
			if (baseTable.boughtTrash[event.target.trash] == 1) then
				if (baseTable.setTrash[event.target.trash] == 1) then
					setButton_text.text = "Switch off"
				else
					setButton_text.text = "Switch on"
				end
			else
				setButton_text.text = "Buy"
			end

			bigCircle.isVisible = true
			bigCircle:toFront()
			transition.to(bigCircle.path, {time = 250, radius = 350})
			transition.to(setButton_group, {time = 200, x = display.contentCenterX + 200})
			transition.to(closeButton_group, {time = 200, x = display.contentCenterX - 200})
			categorie_open = true
			currentCategorie = display.newGroup()
			sceneGroup:insert(currentCategorie)
			for i = 1, #TRASH[event.target.trash] do
				local tempObject = display.newImageRect( sceneGroup, "images/"..TRASH[event.target.trash][i], 100, 100)
				tempObject.x, tempObject.y = math.random(-300/math.sqrt(2), 300/math.sqrt(2)) + display.contentCenterX, math.random(-300/math.sqrt(2), 300/math.sqrt(2)) + display.contentCenterY
				local tempOutline = graphics.newOutline( 15, "images/"..TRASH[event.target.trash][i] )
				physics.addBody(tempObject, {friction=1, bounce=0.1, outline=tempOutline})	
				tempObject.rotation = math.random(0, 360)
				currentCategorie:insert(tempObject)
			end
			bigCircle:toFront()
			setButton_group:toFront()
			circleGroup.isHitTestable = false
			closeButton_group:toFront()
		end
	end

	closeButton:addEventListener("tap", closeCategorie)

	local iconGroup = display.newGroup()
	sceneGroup:insert(iconGroup)

	for i = 1, #CATEGORIES do
		local t
		if (i <= 6) then t = 0 else t = 1 end
		local tempCircle = display.newCircle( sceneGroup, display.contentCenterX + 200*t, 170*(i - t*6 - 1) + 250, 75)
		tempCircle.categorie = i
		tempCircle:setFillColor(128/255)
		tempCircle:addEventListener("tap", openCategorieFood)
		tempCircle:toBack()
		background:toBack()
		local tempIcon = display.newImageRect( sceneGroup, "images/"..CATEGORIES[i][1], 90, 90)
		tempIcon.x, tempIcon.y = tempCircle.x, tempCircle.y
		circleGroup:insert(tempCircle)
		iconGroup:insert(tempIcon)
	end

	for i = 1, #TRASH do
		local tempCircle = display.newCircle( sceneGroup, display.contentCenterX - 200, 170*(i - 1) + 250, 75)
		tempCircle.trash = i
		tempCircle:setFillColor(128/255)
		tempCircle:addEventListener("tap", openCategorieTrash)
		tempCircle:toBack()
		background:toBack()
		local tempIcon = display.newImageRect( sceneGroup, "images/"..TRASH[i][1], 90, 90)
		tempIcon.x, tempIcon.y = tempCircle.x, tempCircle.y
		circleGroup:insert(tempCircle)
		iconGroup:insert(tempIcon)
	end

	local scaleOut = 1

	local function scaleNext()
		if (composer.getSceneName( "current" ) == "scene.shop") then
			for i = 1, iconGroup.numChildren do
				if (scaleOut == 1) then
					transition.to(iconGroup[i], {time = 1000, width = 110, height = 110})
				else
					transition.to(iconGroup[i], {time = 1000, width = 90, height = 90})
				end
			end
			scaleOut = - scaleOut
			local tempTimer = timer.performWithDelay(1000, scaleNext, 1)
		end
	end

	scaleNext()


	local function onAccelerate( event )
		physics.setGravity( 9.8*event.xGravity, -9.8*event.yGravity )
    	print( event.xGravity*9.8, event.yGravity*(-9.8)  )
	end
  
	--Runtime:addEventListener( "accelerometer", onAccelerate )


	local function go_to_menu()
		if (baseTable.sound) then audio.play(clickSound) end
		transition.cancel()
		physics.removeBody(bigCircle)
		Runtime:removeEventListener( "accelerometer", onAccelerate )
		display.remove(currentCategorie)
		if (tempTimer) then timer.cancel(tempTimer) end
		gotoScene("scene.mainMenu","fromLeft")
	end

	local backButton = display.newImageRect( sceneGroup, "images/reply.png", 80, 80)
	backButton.x, backButton.y = display.screenOriginX + 70, 110
	backButton:setFillColor(unpack(TITLE_COLOR))
	backButton:addEventListener("tap",go_to_menu)

	

end

function scene:destroy(event)
	local sceneGroup = self.view
	display.remove(sceneGroup)
	sceneGroup = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("destroy", scene)

return scene
