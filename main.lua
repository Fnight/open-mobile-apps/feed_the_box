--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
======FEEDTHEBOX========
====COPYRIGHT FNIGHT====
========================
]]--

local composer = require("composer")
local json = require("json")
local scene = composer.newScene()

baseTable = {}

try_game_lvl = 0
CATEGORIES = {
	{
	"food_categorie1/candy1.png",
	"food_categorie1/candy2.png",
	"food_categorie1/candy3.png",
	"food_categorie1/candy4.png",
	"food_categorie1/candy5.png",
	"food_categorie1/candy6.png"
	},
	{
	"food_categorie2/candy7.png",
	"food_categorie2/candy8.png",
	"food_categorie2/candy9.png",
	"food_categorie2/candy-cane.png",
	"food_categorie2/candy-cane1.png",
	"food_categorie2/toffee.png"
	},
	{
	"food_categorie3/cupcake.png",
	"food_categorie3/cupcake1.png",
	"food_categorie3/cupcake2.png",
	"food_categorie3/cupcake3.png",
	"food_categorie3/cupcake4.png",
	"food_categorie3/cupcake5.png"
	},
	{
	"food_categorie4/ice-cream.png",
	"food_categorie4/ice-cream1.png",
	"food_categorie4/ice-cream2.png",
	"food_categorie4/ice-cream3.png",
	"food_categorie4/ice-cream4.png",
	"food_categorie4/ice-cream5.png",
	"food_categorie4/ice-cream6.png",
	"food_categorie4/ice-cream7.png"
	},
	{
	"food_categorie5/ice-cream8.png",
	"food_categorie5/ice-cream9.png",
	"food_categorie5/ice-cream10.png",
	"food_categorie5/ice-cream11.png",
	"food_categorie5/ice-cream12.png",
	"food_categorie5/ice-cream13.png",
	"food_categorie5/ice-cream14.png",
	"food_categorie5/ice-cream15.png",
	"food_categorie5/ice-cream16.png"
	},
	{
	"food_categorie6/apple.png",
	"food_categorie6/apple1.png",
	"food_categorie6/banana.png",
	"food_categorie6/cherries.png",
	"food_categorie6/fig.png",
	"food_categorie6/grapes.png",
	"food_categorie6/orange.png",
	"food_categorie6/peach.png"
	},
	{
	"food_categorie7/avocado.png",
	"food_categorie7/peach1.png",
	"food_categorie7/pear.png",
	"food_categorie7/pear1.png",
	"food_categorie7/pineapple.png",
	"food_categorie7/raspberry.png",
	"food_categorie7/strawberry.png",
	},
	{
	"food_categorie8/asparagus.png",
	"food_categorie8/aubergine.png",
	"food_categorie8/cabbage.png",
	"food_categorie8/carrot.png",
	"food_categorie8/tomato.png",
	"food_categorie8/watermelon.png"
	},
	{
	"food_categorie9/bread.png",
	"food_categorie9/croissant.png",
	"food_categorie9/gingerbread-man.png",
	"food_categorie9/pie.png",
	"food_categorie9/pie1.png",
	"food_categorie9/pretzel.png",
	"food_categorie9/toast.png"
	},
	{
	"food_categorie10/hamburger.png",
	"food_categorie10/hot-dog.png",
	"food_categorie10/kebab.png",
	"food_categorie10/pizza.png",
	"food_categorie10/sandwich.png",
	"food_categorie10/sandwich1.png",
	"food_categorie10/taco.png"
	},
	{
	"food_categorie11/ham.png",
	"food_categorie11/hamburguer.png",
	"food_categorie11/pizza1.png",
	"food_categorie11/pizza2.png",
	"food_categorie11/popsicle.png",
	"food_categorie11/sausage.png",
	"food_categorie11/wrap.png"
	},
	{
	"food_categorie12/corndog.png",
	"food_categorie12/doughnut.png",
	"food_categorie12/doughnut1.png",
	"food_categorie12/doughnut2.png",
	"food_categorie12/doughnut3.png"
	}
}

TRASH = {
	{
		"trash_categorie1/american-football.png",
		"trash_categorie1/boomerang.png",
		"trash_categorie1/curling.png",
		"trash_categorie1/nunchaku.png",
		"trash_categorie1/cup.png",
		"trash_categorie1/shoe.png",
		"trash_categorie1/underpants.png",
		"trash_categorie1/whistle.png",
		"trash_categorie1/winter-hat.png"
	},
	{
		"trash_categorie2/blackboard.png",
		"trash_categorie2/briefcase.png",
		"trash_categorie2/diploma.png",
		"trash_categorie2/mortarboard.png",
		"trash_categorie2/notes.png",
		"trash_categorie2/pie-chart.png",
		"trash_categorie2/reader.png",
		"trash_categorie2/pencil.png",
		"trash_categorie2/paper-clips.png"
	},
	{
		"trash_categorie3/axe.png",
		"trash_categorie3/bomb.png",
		"trash_categorie3/bullet-proof-vest.png",
		"trash_categorie3/handcuffs.png",
		"trash_categorie3/sword.png",
		"trash_categorie3/weapong.png",
		"trash_categorie3/hammer.png",
		"trash_categorie3/wrench.png",
		"trash_categorie3/wrench (1).png",
		"trash_categorie3/wrench (2).png",
	},
	{
		"trash_categorie4/bottle-opener.png",
		"trash_categorie4/corkscrew.png",
		"trash_categorie4/fork.png",
		"trash_categorie4/fragile.png",
		"trash_categorie4/teaspoon.png",
		"trash_categorie4/coffees.png",
		"trash_categorie4/drugs.png",
		"trash_categorie4/dustpan.png",
		"trash_categorie4/key.png",
		"trash_categorie4/light-bulb.png",
	},
	{
		"trash_categorie5/armchair.png",
		"trash_categorie5/table.png",
		"trash_categorie5/battery.png",
		"trash_categorie5/printing.png",
		"trash_categorie5/office-chair.png",
		"trash_categorie5/text-lines.png",
		"trash_categorie5/picture.png",
		"trash_categorie5/passport.png",
		"trash_categorie5/desk-lamp.png"
	},
	{
		"trash_categorie6/shampoo.png",
		"trash_categorie6/soap.png",
		"trash_categorie6/soap(2).png",
		"trash_categorie6/sponge.png",
		"trash_categorie6/barrel.png",
		"trash_categorie6/hair-spray.png",
		"trash_categorie6/love-letter.png",
		"trash_categorie6/pill.png"
	}

}

FILE_PATH = system.pathForFile("base.json", system.DocumentsDirectory)


function loadBase()
	local file = io.open(FILE_PATH, "r")

	if file then
		local contents = file:read("*a")
		io.close(file)
		baseTable = json.decode(contents)
	end

--инициализация базы
	if (not(baseTable.boughtCategorie)) then
		baseTable = {
		ads = true,
		music = true,
		sound = true,
		bg = 1,
		recordScore = 0,
		money = 0,
		boughtBackgorund = {1,0,0,0},
		boughtCategorie = {1,0,0,0,0},
		boughtTrash = {1,0,0,0,0},
		setCategorie = {1,0,0,0,0},
		setTrash = {1,0,0,0,0},
		}
	end
end

function saveBase()
	for i = #baseTable, 11, -1 do
        table.remove( baseTable, i )
    end

    local file = io.open( FILE_PATH, "w" )

    if file then
        file:write( json.encode( baseTable ) )
        io.close( file )
    end
end

loadBase()

--Сохраняем процесс музыки на iOS
if (system.getInfo("platformName") == "iPhone OS") then
	local otherAudioIsPlaying = false   
	if (audio.supportsSessionProperty) then
	    audio.setSessionProperty(audio.MixMode, audio.AmbientMixMode)
	    if (audio.getSessionProperty(audio.OtherAudioIsPlaying) ~= 0) then
	        otherAudioIsPlaying = true
	    end
	end
end


clickSound = audio.loadSound()
backgroundMusic = audio.loadStream()
backgroundMusicChannel = audio.play(backgroundMusic, {channel = 3, loops = -1})

if (baseTable.music) then
	audio.resume(backgroundMusicChannel) 
else 
	audio.pause(backgroundMusicChannel) 
end
audio.setVolume(1)

FONT = "Reach Fill.ttf"


function changeMusic()
	if (baseTable.music) then
		baseTable.music = false
		if (soundOn) then audio.play(clickSound) end
		audio.pause(backgroundMusicChannel)
		musicIcon.alpha = 0.5
	else
		baseTable.music = true
		if (soundOn) then audio.play(clickSound) end
		audio.resume(backgroundMusicChannel)
		musicIcon.alpha = 1
	end
	saveBase()
end

function changeSound()
	if (baseTable.sound) then
		baseTable.sound = false
		soundIcon.alpha = 0.5
	else
		baseTable.sound = true
		audio.play(clickSound)
		soundIcon.alpha = 1
	end
	saveBase()
end

function gotoScene(set_scene,set_effect)
	composer.removeScene(set_scene)
	composer.gotoScene(set_scene, {time = 300, effect=set_effect})
end

function goExit()
	if (baseTable.sound) then 
		audio.play(clickSound) 
	end
	native.requestExit()
end


local function onKeyEvent(event)
		if(event.phase == "up" and event.keyName == "back") then
			local curScene = composer.getSceneName( "current" )
			if (curScene == "scene.shop") then
				gotoScene("scene.mainMenu", "fromBottom")
			elseif (curScene == "scene.game") then
				gotoScene("scene.mainMenu", "fromBottom")
			elseif (curScene == "scene.mainMenu") then
				native.requestExit()
			else print("Это не работает") 
			end
			return true
		else 
			return false
		end
		
	end

Runtime:addEventListener("key", onKeyEvent)

function goGame()
	local function deleteSplash()
		display.remove(splash)
	end
	composer.gotoScene("scene.mainMenu", { time=500, effect="crossFade" } )
	transition.to(splash,{time = 500, alpha = 0, onComplete = deleteSplash})
end

splash = display.newImageRect("images/splash.png", 300, 300)
splash.x, splash.y = display.contentCenterX, display.contentCenterY
splash.alpha = 0
if (system.getInfo( "environment" ) == "simulator") then
	transition.to(splash,{time = 0, alpha = 1, onComplete = goGame})
else 
	transition.to(splash,{time = 2500, alpha = 1, onComplete = goGame})
end